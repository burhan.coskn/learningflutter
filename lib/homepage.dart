import 'dart:async';
import 'dart:math';
import 'package:flappy_bird/barrier.dart';
import 'package:flutter/material.dart';
import 'bird.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // Birds

  static double birdY = 0;
  static double birdX = 0;
  double initialPos = birdY;
  double height = 0;
  double time = 0;
  double gravitiy = -4.9; // how strong the gravity?
  double velocity = 1.5; // how strong the Jump ?
  double birdWidth = 0.1; // out of 2, 2 being the entire width of the screen
  double birdHeight = 0.1; // out of 2, 2 being the entire width of the screen
  double randomdouble1Top = Random().nextDouble() * 0.7;
  double randomdouble2Top = Random().nextDouble() * 0.3;
  double randomdouble3Top = Random().nextDouble() * 0.7;
  double randomdouble1Bottom = Random().nextDouble() * 0.3;
  double randomdouble2Bottom = Random().nextDouble() * 0.7;
  double randomdouble3Bottom = Random().nextDouble() * 0.3;
  //generate random double within 0.00 - 1.00;

  // settings
  bool gameHasStarted = false;

  // barrier variables
  static List<double> barrierX = [0.9, 0.9 + 0.8, 1.7 + 0.8];
  static double barrierWidth = 0.35; // out of 2
  List<List<double>> barrierHeight = [
    //Out of 2 where 2 is the entire Height of the screen
    // [topHeight, bottomHeight]
    [0.6, 0.4],
    [0.4, 0.6],
    [0.6, 0.4],
  ];

  void startGame() {
    gameHasStarted = true;
    Timer.periodic(Duration(milliseconds: 50), (timer) {
      height = gravitiy * time * time + velocity * time;

      setState(() {
        birdY = initialPos - height;
      });
      // check if is the bird Dead
      if (birdIsDead()) {
        timer.cancel();
        gameHasStarted = false;
        _showDialog();
      }

      //keep the map Moving(move barriers)
      moveMap();
      // keep the time goings
      time += 0.01;
    });

    Timer.periodic(Duration(milliseconds: 11000), (timer) {
      randomdouble1Top = Random().nextDouble() * 0.8+0.3;
      randomdouble2Top = Random().nextDouble() * 0.3+0.3;
      randomdouble3Top = Random().nextDouble() * 0.7+0.3;
      randomdouble1Bottom = Random().nextDouble() * 0.3+0.3;
      randomdouble2Bottom = Random().nextDouble() * 0.7+0.3;
      randomdouble3Bottom = Random().nextDouble() * 0.3+0.3;


      /*
        if(randomdouble1Bottom<0.3){
        randomdouble1Bottom+=0.25;
      } if(randomdouble2Top<0.3){
        randomdouble2Top+=0.25;
      } if(randomdouble3Top<0.3){
        randomdouble3Top+=0.25;
      } if(randomdouble1Top<0.3){
        randomdouble1Top+=0.25;
      } if(randomdouble2Bottom<0.3){
        randomdouble2Bottom+=0.25;
      } if(randomdouble3Bottom<0.3){
        randomdouble3Bottom+=0.25;
      }
       */
    });
  }

  void jump() {
    setState(() {
      time = 0;
      initialPos = birdY;
    });
  }

  bool birdIsDead() {
    // Check if the bird is hitting the top or bottom of the Screen
    if (birdY < -1 || birdY > 1) {
      return true;
    }

    // check if the bird is hitting the barrier

    for (int i = 0; i < barrierX.length; i++) {
      if (barrierX[i] <= birdWidth &&
          barrierX[i] + barrierWidth >= -birdWidth &&
          (birdY <= -1 + barrierHeight[i][0] ||
              birdY + birdHeight >= 1 - barrierHeight[i][1])) {
        return true;
      }
    }
    return false;
  }

  void moveMap() {
    for (int i = 0; i < barrierX.length; i++) {
      setState(() {
        barrierX[i] -= 0.008;
      });
      if (barrierX[i] < -1.5) {
        barrierX[i] += 2.3;
      }
    }
  }

  void resetGame() {
    Navigator.pop(context); // dismisses the alert dialog
    setState(() {
      birdY = 0;
      birdX = 0;
      gameHasStarted = false;
      time = 0;
      initialPos = birdY;
      barrierX = [0.9, 0.9 + 0.8, 1.7 + 0.8];
    });
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.brown,
            title: Center(
              child: Text(
                'G A M E   O V E R.',
                style: TextStyle(color: Colors.white),
              ),
            ),
            actions: [
              GestureDetector(
                onTap: resetGame,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    padding: EdgeInsets.all(7),
                    color: Colors.white,
                    child: Text(
                      ' PLAY AGAIN',
                      style: TextStyle(color: Colors.brown),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gameHasStarted ? jump : startGame,
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
                flex: 3,
                child: Container(
                  color: Colors.blue,
                  child: Center(
                    child: Stack(
                      children: [
                        MyBird(
                          birdY: birdY,
                          birdX: birdX,
                          birdWidth: birdWidth,
                          birdHeight: birdHeight,
                        ),
                        // top Barrier 0
                        MyBarrier(
                          barrierX: barrierX[0],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[0][0] =
                              randomdouble1Top < 0.2
                                  ? randomdouble1Top + 0.3
                                  : randomdouble1Top,
                          isThisBottomBarrier: false,
                        ),
                        // bottom Barrier 0
                        MyBarrier(
                          barrierX: barrierX[0],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[0][1] =
                              randomdouble1Bottom,
                          isThisBottomBarrier: true,
                        ),
                        // top Barrier 1
                        MyBarrier(
                          barrierX: barrierX[1],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[1][0] = randomdouble2Top,
                          isThisBottomBarrier: false,
                        ),
                        // bottom Barrier 1
                        MyBarrier(
                          barrierX: barrierX[1],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[1][1] =
                              randomdouble2Bottom,
                          isThisBottomBarrier: true,
                        ),
                        // top Barrier 1
                        MyBarrier(
                          barrierX: barrierX[2],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[2][0] = randomdouble3Top,
                          isThisBottomBarrier: false,
                        ),
                        // bottom Barrier 1
                        MyBarrier(
                          barrierX: barrierX[2],
                          barrierWidht: barrierWidth,
                          barrierHeight: barrierHeight[2][1] = randomdouble3Top,
                          isThisBottomBarrier: true,
                        ),
                        Container(
                          alignment: Alignment(0, -0.5),
                          child: Text(
                            gameHasStarted ? '' : ' T A P T O P L A Y ',
                            style: TextStyle(color: Colors.white, fontSize: 10),
                          ),
                        ),
                      ],
                    ),
                  ),
                )),
            Expanded(
                child: Container(
              color: Colors.brown,
            )),
          ],
        ),
      ),
    );
  }
}

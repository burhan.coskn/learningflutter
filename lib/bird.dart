import 'package:flutter/material.dart';

class MyBird extends StatelessWidget {
  final birdY;
  final birdX;
  final double birdWidth;
  final double birdHeight;

  MyBird({this.birdY,this.birdX, required this.birdWidth, required this.birdHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment(birdX, (2 * birdY + birdWidth) / (2 - birdHeight)),
        child: Image.asset(
          'lib/images/bird.png',
          height: MediaQuery.of(context).size.height * birdWidth / 2,
          width: MediaQuery.of(context).size.height * 3 / 4 * birdHeight / 2,
          fit: BoxFit.fill,
        ));
  }
}
